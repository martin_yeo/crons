## The Crons

This repository contains the crons which are to be run regularly in order to maintain the AVD-regulated Scientific Software Stack.

**If you wish to make a change to the crontab, you must submit this via a pull request to https://exxgitrepo:8443/projects/AVD/repos/crons/browse.**

If you change the contents of crontab -l directly, the master cron will automatically delete it and replace it with the crontab in the repository.

---

### Bootstrapping

The master host for AVD crons is **els056**, and all crons are owned by the **avd** user.

To bootstrap the crons process, please perform the following steps:

1. Ensure that you are the **avd** user
   ```shell
   [me@host ~] $ su - avd
   ```
1. Log on to the **els056** host as the **avd** user
   ```shell
   [avd@host ~] $ ssh -Y els056
   ```
1. Change to the `${LOCALTEMP}` base directory on **els056**:
   ```shell
   [avd@els056 ~] $ cd ${LOCALTEMP}
   ```
1. Clone the `crons` Bitbucket repository:
   ```shell
   [avd@els056 avd] $ git clone --branch master ssh://git@exxgitrepo:7999/avd/crons.git avd_crons
   ```
1. Change to the `avd_crons` directory containing the cloned `crons` repository:
   ```shell
   [avd@els056 avd] $ cd avd_crons
   ```
1. Bootstrap the `cronanny` process with the `crontabs` directory:
   ```shell
   [avd@els056 avd_crons] $ ./cronanny.sh crontabs
   ``` 
1. Check that `cronanny` has successfully configured the cronjobs on **els056**:
   ```shell
   [avd@els056 avd_crons] $ crontab -l
   ```
1. The output from the `crontab -l` command should match the contents of the `${LOCALTEMP}/avd_crons/crontabs/els056.avd.crontab` file.
1. For bonus credits, check that `cronanny` has configured the `crontab` for the `host` and `user` of each configuration file contained within the `${LOCALTEMP}/avd_crons/crontabs` directory.

